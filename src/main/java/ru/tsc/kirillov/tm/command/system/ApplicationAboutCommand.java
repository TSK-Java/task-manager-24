package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение информации о разработчике.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Разработчик: Кириллов Максим");
        System.out.println("E-mail: mkirillov@tsconsulting.com");
    }

}
