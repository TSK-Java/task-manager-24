package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "update-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение данных профиля пользователя.";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getUserId();
        System.out.println("[Изменение данных профиля пользователя]");
        System.out.println("Введите имя");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Введите фамилию");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Введите отчество");
        @NotNull final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
