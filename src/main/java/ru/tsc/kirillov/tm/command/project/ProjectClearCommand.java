package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить все проекты.";
    }

    @Override
    public void execute() {
        System.out.println("[Очистка списка проектов]");
        getProjectTaskService().clear(getUserId());
    }

}
