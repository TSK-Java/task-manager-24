package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskCompletedByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-completed-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Завершить задачу по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("Завершение задачи по ID]");
        System.out.println("Введите ID задачи:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(getUserId(), id, Status.COMPLETED);
    }

}
