package ru.tsc.kirillov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    @NotNull
    String SECRET = "wefdG^5yfhjk*9070%6WERGAS35265NIRTtKRTDreasw3$@0(8-";

    @NotNull
    Integer ITERATION = 35345;

    @Nullable
    static String salt(@Nullable final String value) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
